<?php
$nombres = $_GET['nombres'];
$apellidos = $_GET['apellidos'];
$correo = $_GET['correo'];
$telefono = $_GET['telefono'];
$tarjeta = $_GET['tarjeta'];
$ntarjeta = $_GET['ntarjeta'];

require('fpdf/fpdf.php');
$pdf = new FPDF();
$pdf->AddPage();
#cuerpo de pdf
$pdf->image('logo.png' ,10 ,10 ,-500);
$pdf->SetFont('Arial' , 'B' , 16);
$pdf->cell(60);
$pdf->Cell(40, 10, 'comprobante de compra');
$pdf->ln(32);
$pdf->setfont('Arial' , 'B' , 12);
$pdf->cell(0, 5, 'Nombres:' . $nombres, 0, 1);
$pdf->cell(0, 5, 'Apellidos:' . $apellidos, 0, 1);
$pdf->cell(0, 5, 'Correo:' . $correo, 0, 1);
$pdf->cell(0, 5, 'Telefono:' . $telefono, 0, 1);
$pdf->cell(0, 5, 'Tipo de tarjeta:' . $tarjeta, 0, 1);
$pdf->cell(0, 5, 'Numero de la tarjeta:' . $ntarjeta, 0, 1);
$pdf->setfont('Arial' , 'B' , 12);
$pdf->cell(0, 5, 'Acaba de comprar Control Xbox One Wireless Windows PC');
$pdf->ln(20);
$pdf->setfont('Arial' , 'B' , 26);
$pdf->cell(0, 5, 'Gracias por comprar en cp factory');
#fin cuerpo del pdf
$pdf->Output();
?>